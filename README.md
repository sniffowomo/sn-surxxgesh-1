<img src="https://media.tenor.com/FpDM0UZmfuAAAAAM/big-booty.gif" align="right" width="50">
<img src="https://media.tenor.com/FpDM0UZmfuAAAAAM/big-booty.gif" align="left" width="50">

---

<h1 align="center"><code> SN-SURXXXGESH-1 </code></h1>
<h2 align="center"><i></i></h1>

---

1. [W ?](#w-)
2. [Important Commands](#important-commands)
3. [New Projet Commands](#new-projet-commands)

---

# W ?

This repo is for testing the following

```sh
https://surge.sh/
```

- This requies you to have pw, that needs to be recorded seperately since tied to account

# Important Commands

[`Main Docs`](https://surge.sh/help/) explain the installation process as follows

```sh
pnpm i --global surge
```

# New Projet Commands

```sh
pnpm dlx create-next-app@latest
pnpm i -D prettier prettier-plugin-tailwindcss
pnpm i tailwindcss-animated
```

for [`tailwindcss-animate`](https://www.tailwindcss-animated.com/) add

```sh
// tailwind.config.js
module.exports = {
  // ...
  plugins: [
    require('tailwindcss-animated')
  ],
}
```
